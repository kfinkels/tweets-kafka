# Steps
1. Create an App on the Twitter API website. Basically that will give you keys that you need to use the Twitter API.
   https://developer.twitter.com/en/dashboard
2. Create virtual env and activate it:
    ```
    virtualenv env
    sourve env/bin/activate
    ```
3. Install requirements:
    ```
        pip install -r requirements.txt
    ```
4. Start zookeeprt and kafka (this will also create a topic named `trump`):
    ```
        docker-compose up -d
    ``` 
5. Add to your `/etc/hosts` file: 
    ```
   127.0.0.1 kafka
    ```
6. Fill in the access keys you got from your Twitter API account and add them to `tweets.py`:
    ```
        access_token = '<access_token>'
        access_token_secret = '<access_token_secret>'
        consumer_key = '<consumer_key>'
        consumer_secret = '<consumer_secret>'
    ```
7. Run it (data is coming as a stream into the `trump` topic):
    ```
   python tweets.py
   ```
